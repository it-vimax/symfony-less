#!/usr/bin/env bash

#== Import script args ==

timezone=$(echo "$1")

#== Bash helpers ==

function info {
  echo " "
  echo "--> $1"
  echo " "
}

#== Provision script ==

info "Provision-script user: `whoami`"

export DEBIAN_FRONTEND=noninteractive

info "Configure timezone"
timedatectl set-timezone ${timezone} --no-ask-password

info "Prepare root password for MySQL"
debconf-set-selections <<< "mysql-community-server mysql-community-server/root-pass password \"''\""
debconf-set-selections <<< "mysql-community-server mysql-community-server/re-root-pass password \"''\""
echo "Done!"

info "Add PHP repository"
add-apt-repository ppa:ondrej/php -y

info "Add Oracle JDK repository"
add-apt-repository ppa:webupd8team/java -y

info "Update OS software"
apt-get update
apt-get upgrade -y

info "Install additional software"
apt-get install -y unzip nginx mysql-server-5.7
#apt-get install php7.3-common php7.3-fpm php7.3-ldap php7.3-mysqlnd php7.3-mysql php7.3-xml php7.3-xmlrpc php7.3-curl php7.3-gd php7.3-imagick php7.3-cli php7.3-dev php7.3-imap php7.3-mbstring php7.3-opcache php7.3-soap php7.3-zip php7.3-intl -y
apt-get install -y php7.2-curl php7.2-cli php7.2-intl php7.2-mysqlnd php7.2-gd php7.2-fpm php7.2-mbstring php7.2-xml php7.2-zip

apt-get install php-xdebug

info "Install Oracle JDK"
debconf-set-selections <<< "oracle-java8-installer shared/accepted-oracle-license-v1-1 select true"
debconf-set-selections <<< "oracle-java8-installer shared/accepted-oracle-license-v1-1 seen true"
apt-get install -y oracle-java8-installer

info "Install Redis"
apt-get install -y redis-server

info "Install NPM and NodeJs"
apt-get install npm
apt-get install nodejs

info "Install Supervisor"
apt-get install -y supervisor

info "Configure MySQL"
sed -i "s/.*bind-address.*/bind-address = 0.0.0.0/" /etc/mysql/mysql.conf.d/mysqld.cnf
mysql -uroot <<< "CREATE USER 'root'@'%' IDENTIFIED BY ''"
mysql -uroot <<< "GRANT ALL PRIVILEGES ON *.* TO 'root'@'%'"
mysql -uroot <<< "DROP USER 'root'@'localhost'"
mysql -uroot <<< "FLUSH PRIVILEGES"
echo "Done!"

info "Configure PHP-FPM"
#sed -i 's/user = www-data/user = vagrant/g' /etc/php/7.3/fpm/pool.d/www.conf
#sed -i 's/group = www-data/group = vagrant/g' /etc/php/7.3/fpm/pool.d/www.conf
#sed -i 's/owner = www-data/owner = vagrant/g' /etc/php/7.3/fpm/pool.d/www.conf

sed -i 's/user = www-data/user = vagrant/g' /etc/php/7.2/fpm/pool.d/www.conf
sed -i 's/group = www-data/group = vagrant/g' /etc/php/7.2/fpm/pool.d/www.conf
sed -i 's/owner = www-data/owner = vagrant/g' /etc/php/7.2/fpm/pool.d/www.conf
echo "Done!"

info "Configure NGINX"
sed -i 's/user www-data/user vagrant/g' /etc/nginx/nginx.conf
echo "Done!"

info "Enabling site configuration"
ln -s /app/vagrant/nginx/app.conf /etc/nginx/sites-enabled/app.conf
echo "Done!"

info "Set config for Xdebug"
#rm /etc/php/7.3/mods-available/xdebug.ini
#ln -s /app/vagrant/php/xdebug.ini /etc/php/7.3/mods-available/xdebug.ini
rm /etc/php/7.2/mods-available/xdebug.ini
ln -s /app/vagrant/php/xdebug.ini /etc/php/7.2/mods-available/xdebug.ini
sed -i "s/post_max_size = 8M/post_max_size = 50M/g" /etc/php/7.2/fpm/php.ini
sed -i "s/upload_max_filesize = 2M/upload_max_filesize = 50M/g" /etc/php/7.2/fpm/php.ini
echo "Done!"

info "Initailize databases for MySQL"
mysql -uroot <<< "CREATE DATABASE volia_survey"
mysql -uroot <<< "CREATE DATABASE volia_survey_test"
echo "Done!"

info "Enabling supervisor processes"
ln -s /app/vagrant/supervisor/queue.conf /etc/supervisor/conf.d/queue.conf
echo "Done!"

info "Install composer"
curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

info "Install java 8"
apt-get install -y python-software-properties
add-apt-repository ppa:openjdk-r/ppa
apt-get update -y
apt-get install -y openjdk-8-jdk

apt-get update
apt-get install docker.io -y
systemctl start docker
systemctl enable docker
curl -L https://github.com/docker/compose/releases/download/1.21.2/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose

info "Install node.js"
cd ~
curl -sL https://deb.nodesource.com/setup_10.x -o nodesource_setup.sh
bash nodesource_setup.sh
apt-get install nodejs -y

#install make application
apt-get install build-essential -y

# added user vagrant to docker group
groupadd docker
usermod -aG docker vagrant

#info "Up docker-compose"
#cd /app
#docker-compose up -d
