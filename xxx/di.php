<?php

namespace XXX;

class Container
{
    private $definition = [];
    private $singletons = [];

    public function set($name, $value): void
    {
        $this->definition[$name] = $value;
        if (array_key_exists($name, $this->singletons)) {
            unset($this->singletons[$name]);
        }
    }

    public function get($name)
    {
        if (array_key_exists($name, $this->singletons)) {
            return $this->singletons[$name];
        }
        
        if (!array_key_exists($name, $this->definition)) {
            throw new \OutOfBoundsException('Undefined service name ' . $name);
        }

        $definition = $this->definition[$name];
        if ($definition instanceof \Closure) {
            $value = $definition($this);
        } else {
            $value = $definition;
        }

        return $this->singletons[$name] = $value;
    }
}

class TestClass {
    private $name;
    private $password;

    public function __construct($name, $password)
    {
        $this->name = $name;
        $this->password = $password;
    }
}

$container = new Container();

$container->set('db', [
    'dsn' => 'DB1',
    'username' => 'test',
    'password' => '123456',
]);

$container->set('PDO', function (Container $container) {
    $config = $container->get('db');
    return new \PDO($config['dsn'], $config['username'], $config['password']);
});

$container->set('test_class', function (Container $container) {
    $conf = $container->get('db');
    return new TestClass($conf['username'], $conf['password']);
});

$result = $container->get('test_class');

print_r($result);
print_r($container);