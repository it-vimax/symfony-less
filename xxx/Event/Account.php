<?php

namespace XXX\Event;

class Account
{
	private $dispatcher;
	
	private $amount;
	
	/**
	 * Account constructor.
	 * @param int $amount
	 */
	public function __construct(EventDispatcher $dispatcher, int $amount)
	{
		$this->dispatcher = $dispatcher;
		$this->amount = $amount;
	}
	
	
	public function withDraw(int $amount): void
	{
	    if ($amount > $this->amount) {
			throw new \DomainException('Run out from ');
	    }
	    
	    $this->amount -= $amount;
		
		echo "withDraw {$this->amount} amount from" . PHP_EOL;
	    
	    if ($this->amount < 50) {
			$this->dispatcher->dispatch(new BalanceToSmallEvent($this, $this->amount));
		}
	}
	

}
