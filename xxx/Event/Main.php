<?php

namespace XXX\Event;

chdir(dirname(__DIR__) . '/../app');
require 'vendor/autoload.php';

class Main
{
	public static function main()
	{
		$dispatcher = new EventDispatcher();
		$dispatcher->addEventListener(BalanceToSmallEvent::class, function (BalanceToSmallEvent $event) {
			echo 'Balance1 ' . $event->amount . ' is too small from class ' . PHP_EOL;
		});
		$dispatcher->addEventListener(BalanceToSmallEvent::class, function (BalanceToSmallEvent $event) {
			echo 'Balance2 ' . $event->amount . ' is too small from class ' . PHP_EOL;
		});
		
		$acc = new Account($dispatcher, 100);
		
		$acc->withDraw(15);
		$acc->withDraw(15);
		$acc->withDraw(15);
		$acc->withDraw(15);
		$acc->withDraw(15);
		$acc->withDraw(15);
		$acc->withDraw(15);
		$acc->withDraw(15);
		$acc->withDraw(15);
		$acc->withDraw(15);
		$acc->withDraw(15);
		$acc->withDraw(15);
		$acc->withDraw(15);
		$acc->withDraw(15);
	}
}

Main::main();

