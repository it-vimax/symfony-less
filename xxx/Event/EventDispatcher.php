<?php

namespace XXX\Event;

class EventDispatcher
{
	private $listeners = [];
	
	public function addEventListener(string $name, callable $listener): void
	{
		$this->listeners[$name][] = $listener;
	}
	
	public function dispatch(object $event): void
	{
		if (isset($this->listeners[get_class($event)])) {
			foreach ($this->listeners[get_class($event)] as $listener) {
				$listener($event);
			}
		}
	}
}
