<?php

namespace XXX\Event;

class BalanceToSmallEvent
{
	public $target;
	public $amount;
	
	/**
	 * BalanceToSmallEvent constructor.
	 * @param Account $target
	 * @param $amount
	 */
	public function __construct(Account $target, $amount)
	{
		$this->target = $target;
		$this->amount = $amount;
	}
}
