<?php

namespace XXX\Middleware;

class ClientCode
{
	/**
	 * ClientCode constructor.
	 */
	public static function run(Handler $handler)
	{
		foreach (['Nut', 'Banana', 'Cup of coffee'] as $food) {
		    echo "Client: Who wants a " . $food . "?" . PHP_EOL;
			$request = $handler->handle($food);
			if ($request) {
			    echo " " . $request;
			} else {
				echo " " . $food . " was left untouched." . PHP_EOL;
			}
		}
	}
}
