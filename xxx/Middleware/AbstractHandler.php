<?php

namespace XXX\Middleware;

abstract class AbstractHandler implements Handler
{
	/**
	 * @var Handler
	 */
	private $nextHandler;
	
	/**
	 * @param Handler $handler
	 * @return Handler
	 */
	public function setNext(Handler $handler): Handler
	{
	    $this->nextHandler = $handler;
	    return $handler;
	}
	
	public function handle(string $request): ?string
	{
	    if ($this->nextHandler) {
			return $this->nextHandler->handle($request);
	    }
	    return null;
	}
}
