<?php

namespace XXX\Middleware;

chdir(dirname(__DIR__) . '/manager');
require 'vendor/autoload.php';

class Main
{
	public static function main() {
		$monkey = new MonkeyHandler();
		$squirrel = new SquirrelHandler();
		$dog = new DogHandler();
		
		$monkey
			->setNext($squirrel)
			->setNext($dog);
		
		echo "Chain: Monkey > Squirrel > Dog" . PHP_EOL . PHP_EOL;
		ClientCode::run($monkey);
		echo PHP_EOL;
		
		echo "Chain: Squirrel > Dog" . PHP_EOL . PHP_EOL;
		ClientCode::run($squirrel);
		echo PHP_EOL;
	}
}

Main::main();