<?php

namespace XXX\Middleware;

class MonkeyHandler extends AbstractHandler
{
	public function handle(string $request): ?string
	{
	    if ($request === 'Banana') {
	        return "Monkey: 1'll eat the " . $request . PHP_EOL;
	    } else {
			return parent::handle($request);
		}
	}
}
