<?php

namespace XXX;

chdir(dirname(__DIR__) . '/manager');
require 'vendor/autoload.php';

use Doctrine\Common\Annotations\AnnotationRegistry;
use Symfony\Component\Translation\Loader\XliffFileLoader;
use Symfony\Component\Translation\Translator;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\ConstraintViolationInterface;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Symfony\Component\Validator\Validation;

$translator = new Translator('ru');
$translator->addLoader('xlf', new XliffFileLoader());
$translator->addResource('xlf', 'vendor/symfony/validator/Resources/translations/validators.ru.xlf', 'ru', 'validators');

//AnnotationRegistry::registerLoader('class_exists');

$validator = Validation::createValidatorBuilder()
    ->enableAnnotationMapping()
    ->setTranslator($translator)
    ->setTranslationDomain('validators')
    ->getValidator();

class Command {
    /**
     * @var string
     * @Assert\Email()
     * @Assert\NotBlank()
     */
    public $email;
    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\Length(min=6)
     */
    public $password;
}

$command = new Command();
$command->email = 'test#test.loc';
$command->password = '12345';

/** @var ConstraintViolationListInterface|ConstraintViolationInterface[] $violations */
$violations = $validator->validate($command);

if ($violations->count()) {
    foreach ($violations as $violation) {
        echo $violation->getPropertyPath() . ':' . $violation->getMessage() . PHP_EOL;
    }
} else {
    echo 'Command is valid.' . PHP_EOL;
}

