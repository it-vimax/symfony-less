<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210408220338 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP SEQUENCE work_projects_tasks_seq CASCADE');
        $this->addSql('CREATE TABLE work_projects_tasks_executors (task_id INT NOT NULL, member_id UUID NOT NULL, PRIMARY KEY(task_id, member_id))');
        $this->addSql('CREATE INDEX IDX_6291D08E8DB60186 ON work_projects_tasks_executors (task_id)');
        $this->addSql('CREATE INDEX IDX_6291D08E7597D3FE ON work_projects_tasks_executors (member_id)');
        $this->addSql('COMMENT ON COLUMN work_projects_tasks_executors.task_id IS \'(DC2Type:work_projects_task_id)\'');
        $this->addSql('COMMENT ON COLUMN work_projects_tasks_executors.member_id IS \'(DC2Type:work_members_member_id)\'');
        $this->addSql('ALTER TABLE work_projects_tasks_executors ADD CONSTRAINT FK_6291D08E8DB60186 FOREIGN KEY (task_id) REFERENCES work_projects_tasks (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE work_projects_tasks_executors ADD CONSTRAINT FK_6291D08E7597D3FE FOREIGN KEY (member_id) REFERENCES work_members_members (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('DROP TABLE task_project');
        $this->addSql('DROP TABLE work_projects_project_executors');
        $this->addSql('ALTER TABLE work_projects_tasks ADD project_id UUID NOT NULL');
        $this->addSql('ALTER TABLE work_projects_tasks ALTER plan_date TYPE DATE');
        $this->addSql('ALTER TABLE work_projects_tasks ALTER plan_date DROP DEFAULT');
        $this->addSql('ALTER TABLE work_projects_tasks ALTER start_date TYPE DATE');
        $this->addSql('ALTER TABLE work_projects_tasks ALTER start_date DROP DEFAULT');
        $this->addSql('ALTER TABLE work_projects_tasks ALTER end_date TYPE DATE');
        $this->addSql('ALTER TABLE work_projects_tasks ALTER end_date DROP DEFAULT');
        $this->addSql('ALTER TABLE work_projects_tasks ALTER content TYPE TEXT');
        $this->addSql('ALTER TABLE work_projects_tasks ALTER content DROP DEFAULT');
        $this->addSql('COMMENT ON COLUMN work_projects_tasks.project_id IS \'(DC2Type:work_projects_project_id)\'');
        $this->addSql('COMMENT ON COLUMN work_projects_tasks.plan_date IS \'(DC2Type:date_immutable)\'');
        $this->addSql('COMMENT ON COLUMN work_projects_tasks.start_date IS \'(DC2Type:date_immutable)\'');
        $this->addSql('COMMENT ON COLUMN work_projects_tasks.end_date IS \'(DC2Type:date_immutable)\'');
        $this->addSql('ALTER TABLE work_projects_tasks ADD CONSTRAINT FK_E42D1865166D1F9C FOREIGN KEY (project_id) REFERENCES work_projects_projects (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_E42D1865166D1F9C ON work_projects_tasks (project_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE work_projects_tasks_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE task_project (task_id INT NOT NULL, project_id UUID NOT NULL, PRIMARY KEY(task_id, project_id))');
        $this->addSql('CREATE INDEX idx_3052fa04166d1f9c ON task_project (project_id)');
        $this->addSql('CREATE INDEX idx_3052fa048db60186 ON task_project (task_id)');
        $this->addSql('COMMENT ON COLUMN task_project.task_id IS \'(DC2Type:work_projects_task_id)\'');
        $this->addSql('COMMENT ON COLUMN task_project.project_id IS \'(DC2Type:work_projects_project_id)\'');
        $this->addSql('CREATE TABLE work_projects_project_executors (task_id INT NOT NULL, member_id UUID NOT NULL, PRIMARY KEY(task_id, member_id))');
        $this->addSql('CREATE INDEX idx_13935ea88db60186 ON work_projects_project_executors (task_id)');
        $this->addSql('CREATE INDEX idx_13935ea87597d3fe ON work_projects_project_executors (member_id)');
        $this->addSql('COMMENT ON COLUMN work_projects_project_executors.task_id IS \'(DC2Type:work_projects_task_id)\'');
        $this->addSql('COMMENT ON COLUMN work_projects_project_executors.member_id IS \'(DC2Type:work_members_member_id)\'');
        $this->addSql('ALTER TABLE task_project ADD CONSTRAINT fk_3052fa048db60186 FOREIGN KEY (task_id) REFERENCES work_projects_tasks (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE task_project ADD CONSTRAINT fk_3052fa04166d1f9c FOREIGN KEY (project_id) REFERENCES work_projects_projects (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE work_projects_project_executors ADD CONSTRAINT fk_13935ea88db60186 FOREIGN KEY (task_id) REFERENCES work_projects_tasks (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE work_projects_project_executors ADD CONSTRAINT fk_13935ea87597d3fe FOREIGN KEY (member_id) REFERENCES work_members_members (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('DROP TABLE work_projects_tasks_executors');
        $this->addSql('ALTER TABLE work_projects_tasks DROP CONSTRAINT FK_E42D1865166D1F9C');
        $this->addSql('DROP INDEX IDX_E42D1865166D1F9C');
        $this->addSql('ALTER TABLE work_projects_tasks DROP project_id');
        $this->addSql('ALTER TABLE work_projects_tasks ALTER plan_date TYPE TIMESTAMP(0) WITHOUT TIME ZONE');
        $this->addSql('ALTER TABLE work_projects_tasks ALTER plan_date DROP DEFAULT');
        $this->addSql('ALTER TABLE work_projects_tasks ALTER start_date TYPE TIMESTAMP(0) WITHOUT TIME ZONE');
        $this->addSql('ALTER TABLE work_projects_tasks ALTER start_date DROP DEFAULT');
        $this->addSql('ALTER TABLE work_projects_tasks ALTER end_date TYPE TIMESTAMP(0) WITHOUT TIME ZONE');
        $this->addSql('ALTER TABLE work_projects_tasks ALTER end_date DROP DEFAULT');
        $this->addSql('ALTER TABLE work_projects_tasks ALTER content TYPE VARCHAR(255)');
        $this->addSql('ALTER TABLE work_projects_tasks ALTER content DROP DEFAULT');
        $this->addSql('COMMENT ON COLUMN work_projects_tasks.plan_date IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN work_projects_tasks.start_date IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN work_projects_tasks.end_date IS \'(DC2Type:datetime_immutable)\'');
    }
}
