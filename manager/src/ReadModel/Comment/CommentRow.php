<?php

declare(strict_types=1);

namespace App\ReadModel\Comment;

class CommentRow
{
    public $id;
    public $date;
    public $author_id;
    public $author_name;
    public $author_email;
    public $text;
	
	/**
	 * CommentRow constructor.
	 * @param $id
	 * @param $date
	 * @param $author_id
	 * @param $author_name
	 * @param $author_email
	 * @param $text
	 */
	public function __construct($id, $date, $author_id, $author_name, $author_email, $text)
	{
		$this->id = $id;
		$this->date = $date;
		$this->author_id = $author_id;
		$this->author_name = $author_name;
		$this->author_email = $author_email;
		$this->text = $text;
	}
	
	public static function fromArray(array $data): self
	{
		return new self(
			$data['id'],
			$data['date'],
			$data['author_id'],
			$data['author_name'],
			$data['author_email'],
			$data['text']
		);
	}
}
